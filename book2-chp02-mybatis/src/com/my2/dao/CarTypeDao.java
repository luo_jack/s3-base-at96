package com.my2.dao;

import com.my2.entity.CarType;

public interface CarTypeDao{

    CarType getById(int tid);
}
