package com.my2.entity;

import java.util.List;

public class CarType {

    private int tid;

    private String name;

    private List<Car> cars;

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public CarType(int tid, String name) {
        this.tid = tid;
        this.name = name;
    }

    public CarType(String name) {
        this.name = name;
    }

    public CarType() {
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CarType{" +
                "tid=" + tid +
                ", name='" + name + '\'' +
                ", cars=" + cars +
                '}';
    }
}
