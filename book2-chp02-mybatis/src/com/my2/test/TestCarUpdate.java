package com.my2.test;

import com.my2.dao.CarDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class TestCarUpdate {

    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(false);
        CarDao carDao = session.getMapper(CarDao.class);

        try{
            System.out.println(carDao.updateCarPrice(1,103));
            //人为制造异常
            int[] nums = {1,2,3};
            nums[100] = 1;
            System.out.println(carDao.updateCarPrice(2,203));
            //提交事务
            session.commit();
        }catch (Exception ex){
            //回滚事务
            session.rollback();
        }
    }
}
