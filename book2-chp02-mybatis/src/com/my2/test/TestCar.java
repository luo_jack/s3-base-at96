package com.my2.test;

import com.my2.dao.CarDao;
import com.my2.entity.Car;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class TestCar {

    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        CarDao carDao = session.getMapper(CarDao.class);

        //模糊查询
        carDao.getCarByName("").stream().forEach(item -> {
            System.out.println(item);
        });

        //多条件查询
//        Car carQuery = new Car();
//        carQuery.setCarName("u");
//        carQuery.setCarType(1);
//        carDao.getCarByPojo(carQuery).stream().forEach(item -> {
//            System.out.println(item);
//        });

        //多条件查询2
//        HashMap<String,Object> map = new HashMap<>();
//        map.put("carName","");
//        map.put("carType",1);
//        map.put("startPrice",1000);
//        map.put("endPrice",1000000);
//        carDao.getCarByMap(map).stream().forEach(item ->{
//            System.out.println(item);
//        });

        //多条件查询3
//        carDao.getCarByParams("",1).stream().forEach(item ->{
//            System.out.println(item);
//        });
    }
}
