package com.hello;

public class HelloSpring {

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    private String hello = null;

    public void print(){
        System.out.println("Spring say:" + this.getHello() + "!");
    }
}
