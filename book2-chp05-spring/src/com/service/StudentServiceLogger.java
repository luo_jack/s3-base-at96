package com.service;


import org.aspectj.lang.JoinPoint;
public class StudentServiceLogger {

    public void before(JoinPoint joinPoint){
        System.out.println(joinPoint.getTarget()+":"+joinPoint.getSignature().getName()+"被调用");
    }
}
