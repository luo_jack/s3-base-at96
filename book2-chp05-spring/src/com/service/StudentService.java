package com.service;

import com.service.dao.StudentDao;

public class StudentService{

    private StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public void getAll(){
        studentDao.getAll();
    }

    public void getOne(){
        System.out.println("查询单个学生");
    }
}
