package com.jobs.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Job implements Serializable {
//    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
//            `job_title` varchar(80) NOT NULL COMMENT '岗位名称',
//            `category_name` varchar(200) NOT NULL COMMENT '岗位类别名称',
//            `city` varchar(80) NOT NULL COMMENT '城市',
//            `company` varchar(200) NOT NULL COMMENT '用人单位',
//            `company_info` varchar(200) NOT NULL COMMENT '企业类型,经营类型,规模',
//            `author` varchar(200) NOT NULL COMMENT '发布者',
//            `publish_time` varchar(200) NOT NULL COMMENT '发布时间',

    private Integer id;
    private String jobTitle;
    private String categoryName;
    private String city;
    private String company;
    private String companyInfo;
    private String author;
    private Date publishTime;


}
