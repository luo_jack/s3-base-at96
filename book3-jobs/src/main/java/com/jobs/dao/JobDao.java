package com.jobs.dao;

import com.jobs.entity.Job;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface JobDao {

    List<Job> getList(Map<String,Object> params);

    Job getOne(int id);
}
