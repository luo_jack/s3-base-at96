package com.jobs.controller;

import com.github.pagehelper.PageInfo;
import com.jobs.entity.Job;
import com.jobs.service.JobService;
import com.jobs.utils.ResponseResult;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

//@CrossOrigin
@RestController
@RequestMapping("/api")
public class JobController {

    @Autowired
    JobService jobService;

    @GetMapping("/list")
    public ResponseResult getList(@RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
                          @RequestParam(value = "pageSize",defaultValue = "5") int pageSize,
                          String company){
        HashMap<String,Object> param = new HashMap<>();
        param.put("pageNum",pageNum);
        param.put("pageSize",pageSize);
        param.put("company",company);
        PageInfo<Job> jobPageInfo = jobService.getList(param);
        return new ResponseResult(jobPageInfo);
    }

    @GetMapping("/get/{id}")
    public ResponseResult getOne(@PathVariable("id") int id){
        return new ResponseResult(jobService.getOne(id));
    }
}
