package com.jobs.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jobs.dao.JobDao;
import com.jobs.entity.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class JobService {

    @Autowired
    JobDao jobDao;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    public PageInfo<Job> getList(Map<String,Object> params){
        int pageNum = Integer.parseInt(params.get("pageNum").toString());
        int pageSize = Integer.parseInt(params.get("pageSize").toString());
        PageHelper.startPage(pageNum,pageSize);
        List<Job> jobList = jobDao.getList(params);
        return new PageInfo<>(jobList);
    }

    public Job getOne(int id){
        Job job = null;
        String jobId = "job_"+id;
        if(redisTemplate.hasKey(jobId)){
            job = (Job) redisTemplate.opsForValue().get(jobId);
        }else{
            job = jobDao.getOne(id);
            redisTemplate.opsForValue().set(jobId,job,1, TimeUnit.MINUTES);
        }
        return job;
    }
}
