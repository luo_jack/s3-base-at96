package com.jobs;

import com.jobs.service.JobService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@SpringBootApplication
public class Book3JobsApplication {

    public static void main(String[] args) {
        SpringApplication.run(Book3JobsApplication.class, args);
    }

}
