package com.jobs.book3jobs;

import com.github.pagehelper.PageInfo;
import com.jobs.dao.JobDao;
import com.jobs.entity.Job;
import com.jobs.service.JobService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
class Book3JobsApplicationTests {

    @Autowired
    JobDao jobDao;

    @Autowired
    JobService jobService;

    @Autowired
    RedisTemplate redisTemplate;

    @Test
    void daoTest() {
        HashMap<String,Object> param = new HashMap<>();
        List<Job> jobList = jobDao.getList(param);
//        jobList.stream().limit(10).forEach(job -> System.out.println(job));
        //假设
//        Assert.notEmpty(jobList,"xxx");
        assertEquals(3074,jobList.size(),"长度不一致");
    }

    @Test
    void serviceTest(){
        HashMap<String,Object> param = new HashMap<>();
        param.put("pageNum",1);
        param.put("pageSize",5);
        PageInfo<Job> jobPageInfo = jobService.getList(param);
        jobPageInfo.getList().forEach(job -> System.out.println(job));
        System.out.println("getTotal:"+jobPageInfo.getTotal());
        System.out.println("getPages:"+jobPageInfo.getPages());
    }

    @Test
    void testRedis(){
        redisTemplate.opsForValue().set("namename","jim");
        Object obj = redisTemplate.opsForValue().get("namename");
        System.out.println(obj);
    }

    @Test
    void testRedis2(){
        Job job = new Job();
        job.setId(1099);
        job.setJobTitle("好的工资");
        System.out.println(redisTemplate.hasKey("jobjob"));
        redisTemplate.opsForValue().set("jobjob",job,10, TimeUnit.MINUTES);
        Job obj = (Job) redisTemplate.opsForValue().get("jobjob");
        System.out.println(obj);
    }
}
