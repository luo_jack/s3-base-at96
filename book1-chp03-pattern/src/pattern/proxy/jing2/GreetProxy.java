package pattern.proxy.jing2;

public class GreetProxy implements Greeting{

    //被代理对象
    private Greeting greetingObj;

    public GreetProxy(Greeting greeting){
        this.greetingObj = greeting;
    }

    @Override
    public void sayHello(String name) {
        System.out.println("准备开始");
        greetingObj.sayHello(name);
        System.out.println("谢谢大家");
    }
}
