package pattern.proxy.jing2;

public class TestJing2 {

    public static void main(String[] args) {
        Greeting greeting1 = new GreetingImpl();

        greeting1.sayHello("张三");

        Greeting greeting2 = new GreetProxy(greeting1);

        greeting2.sayHello("李四");
    }
}
