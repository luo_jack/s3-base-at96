package pattern.proxy.jing2;

public interface Greeting {

    void sayHello(String name);
}
