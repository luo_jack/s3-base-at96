package pattern.proxy.dong2;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class IntermediaryMethodInterceptor implements MethodInterceptor {
    /**
     * 为被代理的方法定义的代理业务规则
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy)
            throws Throwable {
        before();
        Object feedback = methodProxy.invokeSuper(proxy, args); // 调用父类原始的方法
        after();
        return feedback;
    }

    public void before() {
        System.out.println("大家好");
    }

    public void after() {
        System.out.println("再见");
    }
}