package pattern.proxy.dong2;

import net.sf.cglib.proxy.Enhancer;

public class IntermediaryCglibProxyFactory {
    private static IntermediaryMethodInterceptor callback
            = new IntermediaryMethodInterceptor();
    /**
     * 工厂方法
     * @param target 需要被代理的类型，即代理类需要继承的父类型
     * @return 代理类的实例
     */
    public static <T> T create(Class<T> target) {
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(callback); // 为重写的方法指定回调的MethodInterceptor
        enhancer.setSuperclass(target); // 指定要继承的父类型，即需要被代理的类型
        return (T) enhancer.create(); // 动态生成子类，创建子类实例并返回
    }
}