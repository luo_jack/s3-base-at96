package pattern.proxy.dong2;

public class TestCglib {

    public static void main(String[] args) {
        Student student = IntermediaryCglibProxyFactory.create(Student.class);
        student.sayHello("小明");
    }
}
