package pattern.proxy.dong;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

//日志代理
public class LogHandler implements InvocationHandler {

    //原始被代理对象
    private Object originalObj;

    //返回代理对象
    public Object bind(Object obj){
        this.originalObj = obj;
        return Proxy.newProxyInstance(
                originalObj.getClass().getClassLoader(),
                originalObj.getClass().getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        System.out.println("start:"+method.getName()+":" + System.currentTimeMillis());
        //调用原始被代理对象的方法
        result = method.invoke(originalObj,args);
        System.out.println("end:"+method.getName()+":" + System.currentTimeMillis());
        return result;
    }
}
