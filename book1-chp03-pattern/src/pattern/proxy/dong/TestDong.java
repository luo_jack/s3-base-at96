package pattern.proxy.dong;

import pattern.proxy.jing2.Greeting;
import pattern.proxy.jing2.GreetingImpl;

public class TestDong {

    public static void main(String[] args) {
        Greeting greeting = new GreetingImpl();
        greeting.sayHello("王五");

        Greeting proxy = (Greeting) new LogHandler().bind(greeting);
        proxy.sayHello("赵六");
    }
}
