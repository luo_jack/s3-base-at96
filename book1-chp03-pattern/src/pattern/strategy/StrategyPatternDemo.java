package pattern.strategy;

import pattern.strategy.impl.OperationAdd;
import pattern.strategy.impl.OperationMultiply;
import pattern.strategy.impl.OperationSubtract;

public class StrategyPatternDemo {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.println("10 + 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationSubtract());
        System.out.println("10 - 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationMultiply());
        System.out.println("10 * 5 = " + context.executeStrategy(10, 5));

        context = new Context(new Strategy() {
            @Override
            public int doOperation(int num1, int num2) {
                return num1 / num2;
            }
        });

        System.out.println("10 / 5 = " + context.executeStrategy(10, 5));
    }
}