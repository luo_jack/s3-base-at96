package com.accp.dao;

import java.util.List;

import com.accp.entity.BaseOrder;
import com.accp.entity.OrderInfo;

public interface BaseOrderDao {

	BaseOrder getByOrderNo(String orderNo);
	
	List<BaseOrder> getOrderByUserId(int userId);
	
	//添加订单
	int addBaseOrder(BaseOrder order);
	
	//添加订单明细
	int addOrderInfo(OrderInfo info);
}
