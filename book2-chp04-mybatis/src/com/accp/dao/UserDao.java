package com.accp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.accp.entity.User;

public interface UserDao {

	int update(User user);
	
	int delete(int id);
	
	User getUser(@Param("id") Integer id,@Param("account") String account);
	
	List<User> getUserList();
}
