package com.accp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.accp.entity.Goods;

public interface GoodsDao {

	Goods getById(int id);
	
	Goods getById2(int id);
	
	List<Goods> getGoodsList(@Param("goodsName") String goodsName,
			@Param("leveltype")	int leveltype,@Param("typeid") int typeid);
	
	int updateStock(@Param("id") int id,@Param("stock") int stock);
}
