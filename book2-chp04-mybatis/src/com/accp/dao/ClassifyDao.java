package com.accp.dao;

import com.accp.entity.Classify;

public interface ClassifyDao {

	Classify getById(int id);
}
