package com.accp.entity;

public class Goods {

	private int id;
	
	private String goodsName;
	
	private String goodsDesc;
	
	private double price;
	
	private int stock;
	
	private int classifyLevel1Id;
	
	private int classifyLevel2Id;
	
	private int classifyLevel3Id;
	
	private String fileName;
	
	private boolean isDelete;
	
	private Classify classifyLevel1;
	
	private Classify classifyLevel2;
	
	private Classify classifyLevel3;

	public Classify getClassifyLevel1() {
		return classifyLevel1;
	}

	public void setClassifyLevel1(Classify classifyLevel1) {
		this.classifyLevel1 = classifyLevel1;
	}

	public Classify getClassifyLevel2() {
		return classifyLevel2;
	}

	public void setClassifyLevel2(Classify classifyLevel2) {
		this.classifyLevel2 = classifyLevel2;
	}

	public Classify getClassifyLevel3() {
		return classifyLevel3;
	}

	public void setClassifyLevel3(Classify classifyLevel3) {
		this.classifyLevel3 = classifyLevel3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsDesc() {
		return goodsDesc;
	}

	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getClassifyLevel1Id() {
		return classifyLevel1Id;
	}

	public void setClassifyLevel1Id(int classifyLevel1Id) {
		this.classifyLevel1Id = classifyLevel1Id;
	}

	public int getClassifyLevel2Id() {
		return classifyLevel2Id;
	}

	public void setClassifyLevel2Id(int classifyLevel2Id) {
		this.classifyLevel2Id = classifyLevel2Id;
	}

	public int getClassifyLevel3Id() {
		return classifyLevel3Id;
	}

	public void setClassifyLevel3Id(int classifyLevel3Id) {
		this.classifyLevel3Id = classifyLevel3Id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
