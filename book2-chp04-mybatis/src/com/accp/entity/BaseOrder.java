package com.accp.entity;

import java.util.Date;
import java.util.List;

public class BaseOrder {

	private int id;
	
	private int userId;
	
	private String account;
	
	private String userAddress;
	
	private Date createTime;
	
	private double amount;
	
	private String orderNo;
	
	private List<OrderInfo> orderInfos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Override
	public String toString() {
		return "BaseOrder [id=" + id + ", userId=" + userId + ", account=" + account + ", userAddress=" + userAddress
				+ ", createTime=" + createTime + ", amount=" + amount + ", orderNo=" + orderNo + "]";
	}

	public List<OrderInfo> getOrderInfos() {
		return orderInfos;
	}

	public void setOrderInfos(List<OrderInfo> orderInfos) {
		this.orderInfos = orderInfos;
	}
	
	
}
