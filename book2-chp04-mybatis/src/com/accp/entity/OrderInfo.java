package com.accp.entity;

public class OrderInfo {

	private int id;
	
	private int baseOrderId;
	
	private int goodsId;
	
	private Goods goods;
	
	private int buyNum;
	
	private double amount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBaseOrderId() {
		return baseOrderId;
	}

	public void setBaseOrderId(int baseOrderId) {
		this.baseOrderId = baseOrderId;
	}

	public int getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}


	@Override
	public String toString() {
		return "OrderInfo [id=" + id + ", baseOrderId=" + baseOrderId + ", goodsId=" + goodsId + ", goods=" + goods
				+ ", buyNum=" + buyNum + ", amount=" + amount + "]";
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	
	
}
