package com.accp.entity;

//购物车里的商品信息
public class CartItem {

	private int gid;
	
	private int buyNum;

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public CartItem(int gid, int buyNum) {
		super();
		this.gid = gid;
		this.buyNum = buyNum;
	}

	public CartItem() {
		super();
	}
	
	
}
