package com.accp.test.one;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.UserDao;
import com.accp.entity.User;
import com.accp.utils.MyBatisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

public class Test03 {

	public static void main(String[] args) {
		// 分页查询
		SqlSession session = MyBatisUtil.createSqlSession();
		UserDao userDao = session.getMapper(UserDao.class);
		
		PageHelper.startPage(1, 5);
		List<User> users = userDao.getUserList();
		PageInfo page = new PageInfo<>(users);
		System.out.println("总行数:" + page.getTotal());
		System.out.println("总页数:" + page.getPages());
	}

}
