package com.accp.test.one;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.UserDao;
import com.accp.entity.User;
import com.accp.utils.MyBatisUtil;

public class Test02 {

	public static void main(String[] args) {
		// 更新用户
		SqlSession session = MyBatisUtil.createSqlSession();
		UserDao userDao = session.getMapper(UserDao.class);
		
		User user = userDao.getUser(10,null);
		user.setPassword("1234");
		
		userDao.update(user);
		
		session.commit();
	}

}
