package com.accp.test.three;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.BaseOrderDao;
import com.accp.dao.GoodsDao;
import com.accp.entity.BaseOrder;
import com.accp.entity.OrderInfo;
import com.accp.utils.MyBatisUtil;

public class Test03 {

	public static void main(String[] args) {
		SqlSession session = MyBatisUtil.createSqlSession();
		BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);
		
		// 购买，下订单
		//int[] goodsid = {733,734};
		
		//生成订单
//		BaseOrder baseOrder = new BaseOrder();
//		baseOrder.setUserId(10);
//		baseOrder.setAccount("cgn");
//		baseOrder.setCreateTime(new Date());
//		baseOrder.setAmount(900);
//		baseOrder.setUserAddress("广州");
//		baseOrderDao.addBaseOrder(baseOrder);
//		System.out.println("订单自增号:"+baseOrder.getId());
		//生成订单明细
//		OrderInfo orderInfo = new OrderInfo();
//		orderInfo.setBaseOrderId(999);
//		orderInfo.setGoodsId(100);
//		orderInfo.setBuyNum(2);
//		orderInfo.setAmount(99);
//		baseOrderDao.addOrderInfo(orderInfo);
		//减库存
		//goodsDao.updateStock(733, 199);
		session.commit();
		
	}

}
