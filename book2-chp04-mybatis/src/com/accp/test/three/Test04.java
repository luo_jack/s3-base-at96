package com.accp.test.three;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.BaseOrderDao;
import com.accp.dao.GoodsDao;
import com.accp.entity.BaseOrder;
import com.accp.entity.CartItem;
import com.accp.entity.Goods;
import com.accp.entity.OrderInfo;
import com.accp.utils.MyBatisUtil;

public class Test04 {

	public static void main(String[] args) {
		SqlSession session = MyBatisUtil.createSqlSession();
		BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);

		// 购买，下订单
		// 733 2
		// 734 5
		List<CartItem> items = new ArrayList<CartItem>();
		items.add(new CartItem(733, 3));
		items.add(new CartItem(734, 1));

		List<OrderInfo> orderInfos = new ArrayList<>();
		double total = 0;// 总计
		for (CartItem item : items) {
			Goods goods = goodsDao.getById2(item.getGid());
			double xiaoji = goods.getPrice() * item.getBuyNum();// 小计
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.setGoods(goods);
			orderInfo.setAmount(xiaoji);
			orderInfo.setBuyNum(item.getBuyNum());
			orderInfo.setGoodsId(item.getGid());
			total += xiaoji;
			System.out.println("小计:" + xiaoji);
			orderInfos.add(orderInfo);// 存入订单明细集合
		}
		System.out.println("总计:" + total);

		// 生成订单
		BaseOrder baseOrder = new BaseOrder();
		baseOrder.setUserId(10);
		baseOrder.setAccount("cgn");
		baseOrder.setCreateTime(new Date());
		baseOrder.setUserAddress("广州");
		baseOrder.setAmount(total);// 设置订单总价

		try {
			// 1.插入订单表
			baseOrderDao.addBaseOrder(baseOrder);

			System.out.println("最新的订单流水号:" + baseOrder.getId());
			// 补充设置订单明细的所属的订单流水号
			// 同时插入到数据库
			for (OrderInfo info : orderInfos) {
				info.setBaseOrderId(baseOrder.getId());
				// 2.插入订单明细表
				baseOrderDao.addOrderInfo(info);
				// 扣库存
				Goods goods_info = info.getGoods();
				if(goods_info.getStock()<info.getBuyNum()){
					throw new RuntimeException(goods_info.getGoodsName()+" 商品数量不足");
				}
				goods_info.setStock(goods_info.getStock() - info.getBuyNum());
				// 3.修改商品表
				goodsDao.updateStock(goods_info.getId(), goods_info.getStock());
			}
			session.commit();
		} catch (Exception ex) {
			System.out.println("发生异常");
			ex.printStackTrace();
			session.rollback();
		} finally {
			session.close();
		}
		
	}

}
