package com.accp.test.three;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.BaseOrderDao;
import com.accp.entity.BaseOrder;
import com.accp.entity.OrderInfo;
import com.accp.utils.MyBatisUtil;

public class Test02 {

	public static void main(String[] args) {
		// 查询多个订单
		SqlSession session = MyBatisUtil.createSqlSession();
		BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
		List<BaseOrder> baseOrders = baseOrderDao.getOrderByUserId(2);
		for(BaseOrder baseOrder : baseOrders){
			System.out.println(baseOrder.getOrderNo()+"\t"+baseOrder.getAmount()+"\t"+baseOrder.getCreateTime());
			for(OrderInfo orderInfo : baseOrder.getOrderInfos()){
				System.out.println("\t"+orderInfo.getGoods().getGoodsName()
				+"\t"+orderInfo.getBuyNum()+"\t"+orderInfo.getAmount());
			}
		}
	}

}
