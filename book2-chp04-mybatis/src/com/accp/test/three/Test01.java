package com.accp.test.three;

import java.util.concurrent.SynchronousQueue;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.BaseOrderDao;
import com.accp.dao.GoodsDao;
import com.accp.entity.BaseOrder;
import com.accp.entity.OrderInfo;
import com.accp.utils.MyBatisUtil;

public class Test01 {

	public static void main(String[] args) {
		// 根据订单号查询订单
		SqlSession session = MyBatisUtil.createSqlSession();
		BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);
		BaseOrder baseOrder = baseOrderDao.getByOrderNo("51718726C1274CC59504AB4E6FD64BA0");
		System.out.println(baseOrder);
		for(OrderInfo info : baseOrder.getOrderInfos()){
			//循环查商品
			info.setGoods(goodsDao.getById2(info.getGoodsId()));
		}
		for(OrderInfo info : baseOrder.getOrderInfos()){
			System.out.println(info+","+info.getGoods().getGoodsName());
		}
	}

}
