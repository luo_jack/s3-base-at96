package com.accp.test.two;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.GoodsDao;
import com.accp.entity.Goods;
import com.accp.utils.MyBatisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

public class Test01 {

	public static void main(String[] args) {
		SqlSession session = MyBatisUtil.createSqlSession();
		// 筛选商品
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);
		PageHelper.startPage(1, 3);
		List<Goods> goodslist = goodsDao.getGoodsList("香", 1, 548);
		//goodsDao.getGoodsList("", 2, 656);
		//goodsDao.getGoodsList("香", 0, 0);
		PageInfo page = new PageInfo<>(goodslist);
		System.out.println("总页数:"+page.getPages());
	}

}
