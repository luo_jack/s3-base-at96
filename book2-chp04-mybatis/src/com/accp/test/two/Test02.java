package com.accp.test.two;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.GoodsDao;
import com.accp.entity.Goods;
import com.accp.utils.MyBatisUtil;

public class Test02 {

	public static void main(String[] args) {
		// 查询单个商品
		SqlSession session = MyBatisUtil.createSqlSession();
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);
		Goods goods = goodsDao.getById(733);
		System.out.println("商品名称:" + goods.getGoodsName());
		System.out.println("一级分类:" + goods.getClassifyLevel1Id()+","+goods.getClassifyLevel1().getClassifyName());
		System.out.println("二级分类:" + goods.getClassifyLevel2Id()+","+goods.getClassifyLevel2().getClassifyName());
		System.out.println("三级分类:" + goods.getClassifyLevel3Id()+","+goods.getClassifyLevel3().getClassifyName());
	}

}
