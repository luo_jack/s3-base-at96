package com.accp.test.two;

import org.apache.ibatis.session.SqlSession;

import com.accp.dao.ClassifyDao;
import com.accp.dao.GoodsDao;
import com.accp.entity.Goods;
import com.accp.utils.MyBatisUtil;

public class Test03 {

	public static void main(String[] args) {
		// 查询单个商品
		SqlSession session = MyBatisUtil.createSqlSession();
		GoodsDao goodsDao = session.getMapper(GoodsDao.class);
		ClassifyDao classifyDao = session.getMapper(ClassifyDao.class);
		
		Goods goods = goodsDao.getById2(733);
		goods.setClassifyLevel1(classifyDao.getById(goods.getClassifyLevel1Id()));
		goods.setClassifyLevel2(classifyDao.getById(goods.getClassifyLevel2Id()));
		goods.setClassifyLevel3(classifyDao.getById(goods.getClassifyLevel3Id()));
		System.out.println(goods.getGoodsName());
		System.out.println(goods.getClassifyLevel1().getClassifyName());
		System.out.println(goods.getClassifyLevel2().getClassifyName());
		System.out.println(goods.getClassifyLevel3().getClassifyName());
	}

}
