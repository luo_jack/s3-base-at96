package com.bscp.entity;

import java.util.List;

public class CityArea {
	
	private int id;
	
	private String name;
	
	private List<Branches> brancheslist;//一对多

	@Override
	public String toString() {
		return "CityArea [id=" + id + ", name=" + name + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Branches> getBrancheslist() {
		return brancheslist;
	}

	public void setBrancheslist(List<Branches> brancheslist) {
		this.brancheslist = brancheslist;
	}
	
	

}
