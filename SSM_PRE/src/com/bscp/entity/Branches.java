package com.bscp.entity;

//网点
public class Branches {

	private int id;
	
	private String name;
	
	private int cityAreaId;
	
	//扩展字段
	private String cityAreaName;
	
	private CityArea cityArea;//多对一
	
	private String address;
	
	private String telephone;

	public Branches(int id, String name, int cityAreaId, String address, String telephone) {
		super();
		this.id = id;
		this.name = name;
		this.cityAreaId = cityAreaId;
		this.address = address;
		this.telephone = telephone;
	}
	

	public Branches() {
		super();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCityAreaId() {
		return cityAreaId;
	}

	public void setCityAreaId(int cityAreaId) {
		this.cityAreaId = cityAreaId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	


	@Override
	public String toString() {
		return "Branches [id=" + id + ", name=" + name + ", cityAreaId=" + cityAreaId + ", cityAreaName=" + cityAreaName
				+ ", address=" + address + ", telephone=" + telephone + "]";
	}


	public String getCityAreaName() {
		return cityAreaName;
	}


	public void setCityAreaName(String cityAreaName) {
		this.cityAreaName = cityAreaName;
	}


	public CityArea getCityArea() {
		return cityArea;
	}


	public void setCityArea(CityArea cityArea) {
		this.cityArea = cityArea;
	}
	
	
}
