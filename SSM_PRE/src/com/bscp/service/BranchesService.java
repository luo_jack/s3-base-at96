package com.bscp.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.Branches;

public class BranchesService {
	
	private BranchesDao branchesDao;
	
	public void setBranchesDao(BranchesDao branchesDao) {
		this.branchesDao = branchesDao;
	}
	
	/**
	 * 
	 * @param startPage 第几页
	 * @param pageRowCount 每页多少行
	 * @return
	 */
	public List<Branches> getByStartPage(int startPage,int pageRowCount){
		//计算出从第几行开始
		int startIndex = (startPage - 1) * pageRowCount;
		
		return this.branchesDao.getByStartIndex(startIndex, pageRowCount);
	}
	
	public List<Branches> getByStartPage2(int startPage,int pageRowCount,Branches branches){
		//计算出从第几行开始
		int startIndex = (startPage - 1) * pageRowCount;
		
		return this.branchesDao.getByStartIndex2(startIndex, pageRowCount,branches);
	}
	
	/**
	 * 
	 * @return 总页数
	 */
	public int getPageCount(int pageRowCount){
		int rowsCount = this.branchesDao.getRowsCount();
		if(rowsCount % pageRowCount == 0){
			return rowsCount / pageRowCount;
		}else{
			return rowsCount / pageRowCount + 1;
		}
	}
	
	public int getPageCount2(int pageRowCount,Branches branches){
		int rowsCount = this.branchesDao.getRowsCount2(branches);
		if(rowsCount % pageRowCount == 0){
			return rowsCount / pageRowCount;
		}else{
			return rowsCount / pageRowCount + 1;
		}
	}




}
