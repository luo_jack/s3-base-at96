package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.Branches;

public class TestBranches {

	public static void main(String[] args) throws Exception {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		CityAreaDao cityAreaDao = session.getMapper(CityAreaDao.class);
		BranchesDao branchesDao = session.getMapper(BranchesDao.class);
		
		
		Branches branches = new Branches();
		branches.setId(1);
		branches.setTelephone("888");
		branches.setAddress("广州塔");
		branchesDao.update2(branches);
	

	}

}
