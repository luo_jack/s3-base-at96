package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.Branches;

public class TestIf {

	public static void main(String[] args) throws IOException {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		
		BranchesDao branchesDao = session.getMapper(BranchesDao.class);
		//System.out.println(branchesDao.getByQuery("", 1).size());
		//System.out.println(branchesDao.getByQuery2("1", 2, "西", "87"));
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("cityAreaId", "1");
//		map.put("name", "中");
//		System.out.println(branchesDao.getByQuery3(map));
		
//		Branches branches = new Branches();
//		branches.setCityAreaId(1);
//		branches.setName("中");
//		System.out.println(branchesDao.getByQuery4(branches));
		
//		System.out.println(branchesDao.getByQuery5("", 0));
		
//		List<Integer> nums = new ArrayList<Integer>();
//		nums.add(1);
//		nums.add(3);
//		System.out.println(branchesDao.getByCityAreaIds(nums));
		
		System.out.println(branchesDao.getByQuery6("1", 0, "", ""));
	}

}
