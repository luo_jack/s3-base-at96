package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.entity.Branches;

public class TestMybatis2 {

	public static void main(String[] args) throws Exception {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		int count = session.selectOne("com.bscp.dao.BranchesDao.getcount");
		System.out.println("网点数量:" + count);
		List<Branches> branches = session.selectList("com.bscp.dao.BranchesDao.getAll");
		System.out.println(branches);
	}

}
