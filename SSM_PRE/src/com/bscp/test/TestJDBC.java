package com.bscp.test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.bscp.entity.CityArea;

public class TestJDBC {

	public static void main(String[] args) throws Exception {
		List<CityArea> areas = new ArrayList<CityArea>();
		String connStr = "jdbc:mysql://localhost:3306/exam?useUnicode=true&characterEncoding=UTF-8&useSSL=false&allowPublicKeyRetrieval=true";
		String username = "root";
		String password = "root";
		//连接数据库
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(connStr, username, password);
		//查询 全部的cityarea内容
		String sql = "select id,name from cityarea";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		//遍历resultset
		while(rs.next()){
			CityArea area = new CityArea();
			area.setId(rs.getInt("id"));
			area.setName(rs.getString("name"));
			//生成对象填入集合
			areas.add(area);
		}
		rs.close();
		pst.close();
		conn.close();
		//打印内容
		for(CityArea area : areas){
			System.out.println(area);
		}
	}

}
