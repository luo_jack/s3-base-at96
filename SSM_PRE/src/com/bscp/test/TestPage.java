package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.Branches;
import com.bscp.service.BranchesService;

public class TestPage {

	public static void main(String[] args) throws Exception {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		BranchesDao branchesDao = session.getMapper(BranchesDao.class);
		
		BranchesService branchesService = new BranchesService();
		branchesService.setBranchesDao(branchesDao);
		
		Branches bran = new Branches();
//		bran.setCityAreaId(1);
//		bran.setName("中");
		List<Branches> branches = branchesService.getByStartPage2(1, 2, bran);
		int pageCount = branchesService.getPageCount2(2, bran);
		
//		int pageCount = branchesService.getPageCount(2);
//		List<Branches> branches = branchesService.getByStartPage(1, 2);
		System.out.println("总页数: " + pageCount);
		for(Branches item : branches){
			System.out.println(item);
		}
	}
}
