package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.Branches;
import com.bscp.entity.CityArea;

public class TestMybatis {

	public static void main(String[] args) throws IOException {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		CityAreaDao cityAreaDao = session.getMapper(CityAreaDao.class);
		BranchesDao branchesDao = session.getMapper(BranchesDao.class);
		
		System.out.println(cityAreaDao.getOne(1));
		
		//branchesDao.add2("中原", 1, "洛阳", "123456");
//		HashMap<String, Object> map = new HashMap<>();
//		map.put("name", "华南");
//		map.put("cityAreaId", 1);
//		map.put("address", "同福西路");
//		map.put("telephone", "234233234");
//		branchesDao.add3(map);
	}
}
