package com.bscp.test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.bscp.dao.BranchesDao;
import com.bscp.dao.CityAreaDao;
import com.bscp.entity.CityArea;

public class TestCityArea {

	public static void main(String[] args) throws IOException {
		//读取配置文件
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		//构建session工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		//创建session -- true为自动提交事务，默认为false
		SqlSession session = factory.openSession(true);
		CityAreaDao cityAreaDao = session.getMapper(CityAreaDao.class);
		BranchesDao branchesDao = session.getMapper(BranchesDao.class);
		
		CityArea area = cityAreaDao.getOne(1);
		System.out.println(area);
		System.out.println(area.getBrancheslist());
//		CityArea cityArea = new CityArea();
//		cityArea.setName("测试556");
//		System.out.println(cityArea);
//		cityAreaDao.add(cityArea);
//		System.out.println(cityArea);
	}

}
