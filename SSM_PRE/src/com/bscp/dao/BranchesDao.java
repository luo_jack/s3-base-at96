package com.bscp.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bscp.entity.Branches;

public interface BranchesDao {

	List<Branches> getAll();
	
	List<Branches> getByQueryName(String name);
	
	List<Branches> getByCityAreaId(int cityAreaId);
	
	//查询多个区域下的网点信息
	List<Branches> getByCityAreaIds(List<Integer> areaIds);
	
	List<Branches> getByQuery(@Param("name") String name,
			@Param("cityAreaId") int cityAreaId);
	
	List<Branches> getByQuery2(
			@Param("name") String name,
			@Param("cityAreaId") int cityAreaId,
			@Param("address") String address,
			@Param("telephone") String telephone);
	
	List<Branches> getByQuery3(Map<String,Object> paramsMap);
	
	List<Branches> getByQuery4(Branches branches);
	
	//单一条件查询
	List<Branches> getByQuery5(
			@Param("name") String name,
			@Param("cityAreaId") int cityAreaId);
	
	List<Branches> getByQuery6(
			@Param("name") String name,
			@Param("cityAreaId") int cityAreaId,
			@Param("address") String address,
			@Param("telephone") String telephone);
	
	//元注解
	Branches getOne(@Param("id") int id);
	
	int add(Branches branches);
	
	//不封装，分开传
	int add2(@Param("name") String name,
			@Param("cityAreaId") int cityAreaId,
			@Param("address") String address,
			@Param("telephone") String telephone);
	
	//使用Map封装
	int add3(Map<String,Object> map);
	
	int update(Branches branches);
	
	//部分更新
	int update2(Branches branches);
	
	int delete(@Param("id") int id);
	
	
	//分页
	/**
	 * 
	 * @param start 开始的行号
	 * @param pageRowCount 每页多少行
	 * @return
	 */
	List<Branches> getByStartIndex(@Param("start") int start,@Param("pageRowCount") int pageRowCount);
	
	/**
	 * 
	 * @return 总行数
	 */
	int getRowsCount();
	
	List<Branches> getByStartIndex2(
			@Param("start") int start,
			@Param("pageRowCount") int pageRowCount,
			@Param("branches") Branches branches);
	
	int getRowsCount2(@Param("branches") Branches branches);
}
