package com.bscp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bscp.entity.CityArea;

public interface CityAreaDao {

	List<CityArea> getAll();
	
	CityArea getOne(@Param("id") int id);
	
	int getCount();
	
	int add(CityArea cityArea);
	
	int update(CityArea cityArea);
	
	int delete(@Param("id") int id);
	
}
