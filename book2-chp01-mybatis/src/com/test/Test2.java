package com.test;

import com.entity.CarType;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test2 {

    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);

        List<CarType> carTypeList = session.selectList("com.dao.CarTypeDao.getAll");
        carTypeList.stream().forEach(item -> {
            System.out.println(item.toString());
        });

        InputStream is2 = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory2 = new SqlSessionFactoryBuilder().build(is2);
        SqlSession session2 = factory.openSession(true);
        CarType carType = session2.selectOne("com.dao.CarTypeDao.getById",2);
        System.out.println(carType);
    }
}
