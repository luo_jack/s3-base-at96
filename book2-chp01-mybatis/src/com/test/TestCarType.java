package com.test;

import com.dao.CarTypeDao;
import com.entity.CarType;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class TestCarType {

    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);

        CarTypeDao carTypeDao = session.getMapper(CarTypeDao.class);

//        System.out.println(carTypeDao.getAll());
        System.out.println(carTypeDao.getById(1));
        System.out.println(carTypeDao.getById(1));
//        System.out.println(carTypeDao.add(new CarType("码农吧")));

//        CarType type = carTypeDao.getById(8);
//        type.setName(type.getName()+"123");
//        System.out.println(carTypeDao.update(type));

//        System.out.println(carTypeDao.delete(8));
    }
}
