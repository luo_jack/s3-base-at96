package com.dao;

import com.entity.CarType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CarTypeDao{

    List<CarType> getAll();

    CarType getById(@Param("id") int id);

    int add(CarType carType);

    int update(CarType carType);

    int delete(@Param("id") int id);
}
