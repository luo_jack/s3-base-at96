package com.redis;

import redis.clients.jedis.Jedis;

public class Test1 {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
        jedis.setex("bye",100,"jack");
        String bye = jedis.get("bye");
        System.out.println("bye:" + bye);
    }
}
