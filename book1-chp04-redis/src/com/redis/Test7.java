package com.redis;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

public class Test7 {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
        Student student = new Student(124,"码农",19);
        Gson gson = new Gson();
        String json = gson.toJson(student);
        System.out.println(json);
        jedis.set("stu124",json);

        String json2 = jedis.get("stu124");
        Student student2 = gson.fromJson(json2,Student.class);
        System.out.println(student2);
    }
}
