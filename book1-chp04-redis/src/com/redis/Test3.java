package com.redis;

import redis.clients.jedis.Jedis;

import java.util.Arrays;

public class Test3 {

    public static void main(String[] args) {
        Student student = new Student(102,"杰克",19);
        //把student对象存入redis
        byte[] arr = SerializeUtils.serialize(student);
        System.out.println(Arrays.toString(arr));
        Jedis jedis = new Jedis("localhost",6379);
        jedis.set("stu102".getBytes(),arr);
        jedis.close();
    }
}
