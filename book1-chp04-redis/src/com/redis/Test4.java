package com.redis;

import redis.clients.jedis.Jedis;

import java.util.Arrays;

public class Test4 {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
        byte[] arr = jedis.get("stu102".getBytes());
        System.out.println(Arrays.toString(arr));
        //读取redis中的学生对象
        Student student = (Student) SerializeUtils.deSerialize(arr);
        System.out.println(student);
    }
}
