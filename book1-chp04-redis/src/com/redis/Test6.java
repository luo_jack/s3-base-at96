package com.redis;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class Test6{
    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
        List<Student> students = new ArrayList<>();
        students.add(new Student(101,"jack",19));
        students.add(new Student(102,"啊",19));
        students.add(new Student(103,"发",19));
        jedis.set("stu".getBytes(),SerializeUtils.serialize(students));

        List<Student> students2 = (List<Student>) SerializeUtils.deSerialize(jedis.get("stu".getBytes()));
        System.out.println(students2);
    }
}
