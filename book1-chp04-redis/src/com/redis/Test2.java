package com.redis;

import redis.clients.jedis.Jedis;

public class Test2 {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
        boolean flag = jedis.exists("bye");
        System.out.println("是否存在:" + flag);
        Long ttl = jedis.ttl("bye");
        System.out.println("ttl:" + ttl);
        if(flag){
            System.out.println(jedis.get("bye"));
        }
    }
}
