package com.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * JedisAPI
 */
public class JedisAPI {

	private static JedisPool jedisPool;

	static {
		// 创建并设置连接池配置对象
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(50); 		// 连接池最大连接数，默认为8
		config.setMaxIdle(10); 			// 连接池中的最大空闲连接，默认为8
		config.setMinIdle(7); 			// 连接池中的最小空闲连接，默认为0
		config.setMaxWaitMillis(1000);	// 获取资源的等待时间
		config.setTestOnBorrow(true);	// 获取资源时是否验证资源的有效性

		// 创建Jedis连接池
		jedisPool = new JedisPool(config,		// 连接池配置对象
								"localhost",	// Redis服务器地址
								6379,			// Redis服务端口
								10000,			// 连接超时时间，单位毫秒，默认2000ms即2s
								null,		// Redis密码
								0);				// 数据库索引
	}
	
	public void destroy() {
		if (!(jedisPool == null || jedisPool.isClosed()))
			jedisPool.close();	// 关闭连接池
	}

	/**
	 * set key and value to redis
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean set(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}
	}

	/**
	 * set key and value to redis
	 * 
	 * @param key
	 * @param seconds
	 *            有效期
	 * @param value
	 * @return
	 */
	public boolean set(String key, int seconds, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.setex(key, seconds, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}
		return false;
	}

	/**
	 * 判断某个key是否存在
	 * 
	 * @param key
	 * @return
	 */
	public boolean exist(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.exists(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}
		return false;
	}

	/**
	 * 获取数据
	 * 
	 * @param key
	 * @return
	 */
	public String get(String key) {
		String value = null;
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			value = jedis.get(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}

		return value;
	}

	/**
	 * 查询key的有效期,当 key 不存在时，返回 -2 。 当 key 存在但没有设置剩余生存时间时，返回 -1 。 否则，以秒为单位，返回 key
	 * 的剩余生存时间。 注意：在 Redis 2.8 以前，当 key 不存在，或者 key 没有设置剩余生存时间时，命令都返回 -1 。
	 * 
	 * @param key
	 * @return 剩余多少秒
	 */
	public Long ttl(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.ttl(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}
		return (long) -2;
	}

	/**
	 * 删除
	 * 
	 * @param key
	 */
	public void delete(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.del(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			if (jedis != null)
				jedis.close();
		}
	}
}
