package com.service.dao;

import org.springframework.stereotype.Component;

@Component
public class StudentDaoOracle implements StudentDao {
    @Override
    public void getAll() {
        System.out.println("oracle返回 学生列表");
    }
}
