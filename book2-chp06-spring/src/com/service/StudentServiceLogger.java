package com.service;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class StudentServiceLogger {

    @Before("execution(* com.service.StudentService.get*(..))")
    public void before(JoinPoint joinPoint) {
        System.out.println(joinPoint.getTarget() + ":" + joinPoint.getSignature().getName() + "被调用");
    }
}
