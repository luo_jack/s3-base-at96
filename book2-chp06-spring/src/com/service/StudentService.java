package com.service;

import com.service.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class StudentService {

    @Autowired
    @Qualifier("studentDaoMySql")
    private StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public void getAll() {
        studentDao.getAll();
    }

    public void getOne() {
        System.out.println("查询单个学生");
    }
}
