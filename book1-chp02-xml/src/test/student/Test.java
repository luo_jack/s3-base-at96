package test.student;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Test {

    public static void main(String[] args) throws Exception {
        SAXReader saxReader = new SAXReader();

        String path = Test.class.getClassLoader().getResource("student.xml").getPath();
        Document doc = saxReader.read(path);
        Element root = doc.getRootElement();
        System.out.println(root);
        Element element = doc.elementByID("2");
        System.out.println(element.elementText("name"));
    }
}
