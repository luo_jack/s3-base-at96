package test.student;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class StudentManager {

    Scanner input = new Scanner(System.in);

    final String filename = "student.xml";

    String xmlpath = StudentManager.class.getClassLoader().getResource(filename).getPath();

    public void showMenu() throws Exception {
        //1.查询全部学生
        //2.新增学生
        //3.修改学生年龄
        //4.删除学生
        //0.退出系统
        while (true) {

            System.out.println("\n1.查询全部学生");
            System.out.println("2.新增学生");
            System.out.println("3.修改学生年龄");
            System.out.println("4.删除学生");
            System.out.println("0.退出系统");
            System.out.print("请选择:");
            String choose = input.next();
            if (choose.equals("1")) {
                showAll();
            } else if (choose.equals("2")) {
                add();
            }else if (choose.equals("3")) {
                update();
            }else if (choose.equals("4")) {
                delete();
            }else if (choose.equals("0")) {
                break;
            }
        }
    }

    public void showAll() throws Exception {
        Document doc = readxml();
        Element root =doc.getRootElement();
        for(Element elementStu: root.elements("student")){
            System.out.println(elementStu.attributeValue("ID")+"\t"+
                    elementStu.elementText("name")+"\t"+
                    elementStu.elementText("age") + "\t" +
                    elementStu.elementText("gender"));

        }
    }

    public void add() throws DocumentException, IOException {
        System.out.print("请输入姓名:");
        String name = input.next();
        System.out.print("请输入年龄:");
        int age = input.nextInt();
        System.out.print("请输入性别:");
        String gender = input.next();

        Document doc = readxml();
        Element root =doc.getRootElement();
        int index = Integer.parseInt(root.attributeValue("index"));
        index++;
        root.addAttribute("index",String.valueOf(index));

        Element elementStudent = root.addElement("student");
        elementStudent.addAttribute("ID", String.valueOf(index));
        elementStudent.addElement("name").addText(name);
        elementStudent.addElement("age").addText(String.valueOf(age));
        elementStudent.addElement("gender").addText(gender);

        writexml(doc);
        System.out.println("添加成功");
    }

    public void update() throws DocumentException, IOException {
        System.out.print("请输入学生ID:");
        int id = input.nextInt();

        Document doc = readxml();
        Element elementStudent = doc.elementByID(String.valueOf(id));
        if (elementStudent == null){
            System.out.println("没有该学生信息");
        }else{
            System.out.println("当前的年龄:" + elementStudent.elementText("age"));
            System.out.print("请输入新的年龄:");
            int age = input.nextInt();
            elementStudent.element("age").setText(String.valueOf(age));
            writexml(doc);
            System.out.println("修改成功");
        }
    }

    public void delete() throws DocumentException, IOException {
        System.out.print("请输入需要删除的学生ID:");
        int id = input.nextInt();

        Document doc = readxml();
        Element elementStudent = doc.elementByID(String.valueOf(id));
        if (elementStudent == null){
            System.out.println("没有该学生信息");
        }else{
            elementStudent.getParent().remove(elementStudent);
            writexml(doc);
            System.out.println("删除成功");
        }
    }

    public static void main(String[] args) throws Exception {
        StudentManager studentManager = new StudentManager();
        studentManager.showMenu();
    }

    private Document readxml() throws DocumentException {
        SAXReader saxReader = new SAXReader();
        Document doc = saxReader.read(xmlpath);
        return doc;
    }

    private void writexml(Document doc) throws IOException {
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(new FileWriter(xmlpath),format);
        writer.write(doc);
        writer.close();
    }
}
