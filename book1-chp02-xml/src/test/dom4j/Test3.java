package test.dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import test.dom.Test4;
import test.entity.Phone;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Test3 {

    public static void main(String[] args) throws Exception {
        System.out.println("Dom4j Test3");
        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("网易手机各地行情.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);
        Element root = doc.getRootElement();
        List<Phone> phoneList = new ArrayList<>();
        for (Element elementBrand:root.elements("Brand")) {
            Phone phone = new Phone();
            phoneList.add(phone);
            phone.setBrand(elementBrand.attributeValue("name"));
            for (Element elementType: elementBrand.elements("type")) {
                phone.setType(elementType.attributeValue("name"));
                Element elementItem = elementType.element("item");
                phone.setTitle(elementItem.elementText("title"));
                phone.setLink(elementItem.elementText("link"));
                phone.setDescription(elementItem.elementText("description"));
                phone.setPubDate(elementItem.elementText("pubDate"));
            }
        }

        phoneList.stream().forEach(phone -> {
            System.out.println(phone);
        });
    }
}
