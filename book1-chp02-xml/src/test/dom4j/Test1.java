package test.dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import test.dom.Test4;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Test1 {

    public static void main(String[] args) throws Exception {
        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("收藏信息.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);
        Element root = doc.getRootElement();
        System.out.println(root);
        List<Element> brands = root.elements("Brand");
        for (Element brand:brands) {
            System.out.println("Brand " + brand.attributeValue("name"));
            List<Element> types = brand.elements("Type");
            for (Element type:types) {
                System.out.println("\tType " + type.attributeValue("name"));
            }
        }
    }
}
