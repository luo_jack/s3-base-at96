package test.dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class Test4 {

    public static void main(String[] args) throws Exception {
        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = test.dom.Test4.class.getClassLoader().getResource("收藏信息.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);
        Element root = doc.getRootElement();
        System.out.println(root);
        Element elementBrand = root.addElement("Brand");
        elementBrand.addAttribute("name","四星");

        Element elementType = elementBrand.addElement("Type");
        elementType.addAttribute("name","NoteXXX");

        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(new FileWriter(decodedPath),format);
        writer.write(doc);
        writer.close();
    }
}
