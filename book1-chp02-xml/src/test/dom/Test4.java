package test.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class Test4 {

    public static void main(String[] args) throws Exception {
        //添加
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("收藏信息.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());

        // 解析XML文档
        Document doc = db.parse(decodedPath);

        // 获取根节点并添加新元素
        Element root = doc.getDocumentElement();
        Element element1 = doc.createElement("Brand");
        element1.setAttribute("name", "三星");
        root.appendChild(element1);
        Element element2 = doc.createElement("Type");
        element2.setAttribute("name","NoteX");
        element1.appendChild(element2);
        // 保存XML文件
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        // 使用解码后的路径保存文件
        StreamResult result = new StreamResult(new FileOutputStream(decodedPath));
        transformer.transform(new DOMSource(doc), result);
    }
}
