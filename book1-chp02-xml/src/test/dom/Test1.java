package test.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Test1 {

    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        String path = Test1.class.getClassLoader().getResource("收藏信息.xml").getPath();
        //获取到文档对象
        Document doc = db.parse(path);
        //获取根节点
        Element root = doc.getDocumentElement();
        System.out.println("根节点:" + root.getNodeName());
        NodeList nodeList = root.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++){
            if(nodeList.item(i).getNodeType()==1) {
                Element element = (Element) nodeList.item(i);
                System.out.println(element.getAttribute("name"));
                NodeList nodeListType = element.getElementsByTagName("Type");
                for (int j = 0; j < nodeListType.getLength(); j++){
                    Element elementType = (Element) nodeListType.item(j);
                    System.out.println("\t"+elementType.getAttribute("name"));
                }
            }
        }
    }
}
