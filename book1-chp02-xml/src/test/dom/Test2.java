package test.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Test2 {

    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        String path = Test2.class.getClassLoader().getResource("网易手机各地行情.xml").getPath();
        //获取到文档对象
        Document doc = db.parse(path);
        //获取根节点
        Element root = doc.getDocumentElement();
        System.out.println("根节点:" + root.getNodeName());
        NodeList nodeListBrand = root.getElementsByTagName("Brand");
        for (int i = 0; i < nodeListBrand.getLength(); i++){
            Element elementBrand = (Element) nodeListBrand.item(i);
            System.out.println(elementBrand.getAttribute("name"));
            NodeList nodeListType = elementBrand.getElementsByTagName("type");
            for(int j = 0; j < nodeListType.getLength(); j++){
                Element elementType = (Element) nodeListType.item(j);
                Element elementItem = (Element) elementType.getFirstChild().getNextSibling();
                NodeList nodeList = elementItem.getChildNodes();
                System.out.println("------------------");
                for (int k = 0; k < nodeList.getLength(); k++){
                    if(nodeList.item(k).getNodeType() == 1){
                        System.out.println("\t"+nodeList.item(k).getNodeName()+"\t"+nodeList.item(k).getTextContent());
                    }
                }
            }
        }
    }
}
