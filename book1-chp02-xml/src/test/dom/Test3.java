package test.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import test.entity.Phone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

public class Test3 {

    public static void main(String[] args) throws Exception {
        List<Phone> phones = new ArrayList<>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        String path = Test3.class.getClassLoader().getResource("网易手机各地行情.xml").getPath();
        //获取到文档对象
        Document doc = db.parse(path);
        //获取根节点
        Element root = doc.getDocumentElement();
        System.out.println("根节点:" + root.getNodeName());
        NodeList nodeListBrand = root.getElementsByTagName("Brand");
        for (int i = 0; i < nodeListBrand.getLength(); i++){
            Element elementBrand = (Element) nodeListBrand.item(i);
//            System.out.println(elementBrand.getAttribute("name"));
            NodeList nodeListType = elementBrand.getElementsByTagName("type");
            for(int j = 0; j < nodeListType.getLength(); j++){
                Phone phone = new Phone();
                phones.add(phone);
                phone.setBrand(elementBrand.getAttribute("name"));
                Element elementType = (Element) nodeListType.item(j);
                phone.setType(elementType.getAttribute("name"));
                Element elementItem = (Element) elementType.getFirstChild().getNextSibling();
                NodeList nodeList = elementItem.getChildNodes();

                phone.setTitle(
                        elementItem.getElementsByTagName("title").item(0).getTextContent()
                );
                phone.setLink(
                        elementItem.getElementsByTagName("link").item(0).getTextContent()
                );
                phone.setDescription(
                        elementItem.getElementsByTagName("description").item(0).getTextContent()
                );
                phone.setPubDate(
                        elementItem.getElementsByTagName("pubDate").item(0).getTextContent()
                );
            }
        }

//        for (Phone phone:phones) {
//            System.out.println(phone);
//        }
        //lamda表示式
        phones.stream().forEach((phone) -> {
            System.out.println(phone);
        });
    }
}
