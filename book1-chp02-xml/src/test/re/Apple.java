package test.re;

public class Apple {

    public void a(){
        System.out.println("Apple...a");
    }
    public void b(){
        System.out.println("Apple...b");
    }
    public void c(){
        System.out.println("Apple...c");
    }

    public void d(int a){
        System.out.println("Apple...d1:" + a);
    }

    public void d(int a,int b){
        System.out.println("Apple...d2: a:" + a + " b:" + b);
    }

    public void d(int a,String b){
        System.out.println("Apple...d3: a:" + a + " b:" + b);
    }
}
