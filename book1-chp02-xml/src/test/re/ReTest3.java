package test.re;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import test.dom.Test4;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ReTest3 {

    public static void main(String[] args) throws Exception {
//        Apple apple = new Apple();
//        Class clz = apple.getClass();
//        Class[] classes = {};
//        Method method = clz.getDeclaredMethod("c",classes);
//        Object[] params = {};
//        method.invoke(apple,params);

        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("test3.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);

        for (Node node1: doc.selectNodes("//method")) {
            Element elementNode = (Element) node1;
            String methodName = elementNode.attributeValue("name");
            String className = elementNode.attributeValue("class");
            Class clz = Class.forName(className);
            Object obj = clz.newInstance();

            List<Class> classes = new ArrayList<>();
            List<Object> params = new ArrayList<>();
            List<Element> elementList = elementNode.elements("param");
//            Class[] classes = new Class[elementList.size()];
//            Object[] params = new Object[elementList.size()];

            //获取参数列表
            for (int i = 0; i < elementList.size(); i++){
                Element elementParam = elementList.get(i);
                String type = elementParam.attributeValue("type");
                String value = elementParam.attributeValue("value");
//                classes[i] = getClass(type);
//                params[i] = getValue(value,classes[i]);
                classes.add(getClass(type));
                params.add(getValue(value,classes.get(i)));
            }
            Method method = clz.getDeclaredMethod(methodName, classes.toArray(new Class[0]));
            method.invoke(obj,params.toArray());
        }
    }

    private static Object getValue(String value,Class clz){
        if(clz == int.class){
            return Integer.parseInt(value);
        }else if(clz == String.class){
            return value;
        }else{
            return value;
        }
    }

    private static Class getClass(String type){
        if(type.equals("int")){
            return int.class;
        }else if(type.equals("String")){
            return String.class;
        }
        return Object.class;
    }
}
