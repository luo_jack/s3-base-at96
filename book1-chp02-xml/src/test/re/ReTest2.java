package test.re;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import test.dom.Test4;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class ReTest2 {
    public static void main(String[] args) throws Exception {
        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("test2.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);

        for (Node node1: doc.selectNodes("//method")) {
            Element elementNode = (Element) node1;
            String methodName = elementNode.attributeValue("name");
            String className = elementNode.attributeValue("class");
            Class clz = Class.forName(className);
            Object obj = clz.newInstance();
            clz.getDeclaredMethod(methodName).invoke(obj);
        }
    }
}
