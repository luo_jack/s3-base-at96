package test.re;

public class Pear {

    public void a(){
        System.out.println("Pear...a");
    }
    public void b(){
        System.out.println("Pear...b");
    }
    public void c(){
        System.out.println("Pear...c");
    }

    public void d(int a){
        System.out.println("Pear...d1:" + a);
    }

    public void d(int a,int b){
        System.out.println("Pear...d2: a:" + a + " b:" + b);
    }

    public void d(int a,String b){
        System.out.println("Pear...d3: a:" + a + " b:" + b);
    }
}
