package test.re;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import test.dom.Test4;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class ReTest1 {

    public static void main(String[] args) throws Exception {
        Apple apple = new Apple();
        SAXReader saxReader = new SAXReader();

        // 获取资源路径并进行URL解码
        String path = Test4.class.getClassLoader().getResource("test1.xml").getPath();
        String decodedPath = URLDecoder.decode(path, StandardCharsets.UTF_8.name());
        Document doc = saxReader.read(decodedPath);
        Class clz = Apple.class;
        for (Node node1: doc.selectNodes("//method")) {
           Element elementNode = (Element) node1;
           String methodName = elementNode.attributeValue("name");
           clz.getDeclaredMethod(methodName).invoke(apple);
        }
    }
}
