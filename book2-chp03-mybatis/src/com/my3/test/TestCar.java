package com.my3.test;

import com.my3.dao.CarDao;
import com.my3.entity.Car;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestCar {

    public static void main(String[] args) throws IOException {
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        CarDao carDao = session.getMapper(CarDao.class);

//        Map<String,Object> param = new HashMap<>();
//        param.put("carName","省");
//        param.put("carType",2);
//        param.put("startPrice",300);
//        param.put("endPrice",600000);
//        carDao.getByQuery2(param).stream().forEach(item -> {
//            System.out.println(item);
//        });

//        carDao.getByCarType(new ArrayList<Integer>(Arrays.asList(1,2,7,8,9))).forEach(item -> {
//            System.out.println(item);
//        });

        Car car = new Car();
        car.setCarId(1);
        car.setCarPrice(999.0);
        car.setCarStatus(0);
        carDao.updateByChange(car);
    }
}
