package com.my3.test;

import com.my3.dao.JobDao;
import com.my3.entity.Job;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestJob {

    public static void main(String[] args) throws IOException, ParseException {
        //页数，从1开始
        int pageNum = 1;
        //每页行数
        int pageSize = 5;

        //总行数
        int total = 0;
        //总页数
        int pages = 0;

        //自顶而下设计，自底到上写代码
        //读取配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //构建session工厂
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);

        JobDao jobDao = session.getMapper(JobDao.class);
        Map<String,Object> param = new HashMap<>();
        param.put("jobTitle","高");
        param.put("categoryName","web");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        param.put("startTime",simpleDateFormat.parse("2024-2-1"));
        param.put("endTime",simpleDateFormat.parse("2024-3-1"));

        total = jobDao.getTotal(param);
        if(total % pageSize == 0){
            pages = total / pageSize;
        }else{
            pages = total / pageSize + 1;
        }
        //限制页数范围
        if(pageNum<1){
            pageNum = 1;
        }else if(pageNum > pages){
            pageNum = pages;
        }
        param.put("pageNum",pageNum);
        param.put("pageSize",pageSize);
        List<Job> jobs = jobDao.getList(param);
        System.out.println("总行数:"+total);
        System.out.println("总页数:"+pages);
        System.out.println("当前页:" + pageNum);
        jobs.forEach(item -> {
            System.out.println(item);
        });
    }
}
