package com.my3.dao;

import com.my3.entity.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CarDao {

    List<Car> getByQuery(Map<String,Object> param);

    List<Car> getByQuery2(Map<String,Object> param);

    List<Car> getByCarType(List<Integer> types);

    void updateByChange(Car car);
}
