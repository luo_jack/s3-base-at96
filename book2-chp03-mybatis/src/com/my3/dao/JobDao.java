package com.my3.dao;

import com.my3.entity.Job;

import java.util.List;
import java.util.Map;

public interface JobDao {

    List<Job> getList(Map<String,Object> param);

    int getTotal(Map<String,Object> param);
}
