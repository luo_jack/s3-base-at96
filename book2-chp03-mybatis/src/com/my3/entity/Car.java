package com.my3.entity;

import java.util.Date;

public class Car {

    private Integer carId;
    private String carName;
    private Double carPrice;
    private String carMileage;
    private Date carBuyTime;
    private Integer carType;
    private Integer carStatus;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Double getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(Double carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarMileage() {
        return carMileage;
    }

    public void setCarMileage(String carMileage) {
        this.carMileage = carMileage;
    }

    public Date getCarBuyTime() {
        return carBuyTime;
    }

    public void setCarBuyTime(Date carBuyTime) {
        this.carBuyTime = carBuyTime;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public Integer getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(Integer carStatus) {
        this.carStatus = carStatus;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", carName='" + carName + '\'' +
                ", carPrice=" + carPrice +
                ", carMileage='" + carMileage + '\'' +
                ", carBuyTime=" + carBuyTime +
                ", carType=" + carType +
                ", carStatus=" + carStatus +
                '}';
    }
}
