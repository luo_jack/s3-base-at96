package com.my3.entity;

import java.util.Date;

public class Job {
//    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
//            `job_title` varchar(80) NOT NULL COMMENT '岗位名称',
//            `category_name` varchar(200) NOT NULL COMMENT '岗位类别名称',
//            `city` varchar(80) NOT NULL COMMENT '城市',
//            `company` varchar(200) NOT NULL COMMENT '用人单位',
//            `company_info` varchar(200) NOT NULL COMMENT '企业类型,经营类型,规模',
//            `author` varchar(200) NOT NULL COMMENT '发布者',
//            `publish_time` varchar(200) NOT NULL COMMENT '发布时间',

    private Integer id;
    private String jobTitle;
    private String categoryName;
    private String city;
    private String company;
    private String companyInfo;
    private String author;
    private Date publishTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = companyInfo;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", jobTitle='" + jobTitle + '\'' +
                ", CategoryName='" + categoryName + '\'' +
                ", city='" + city + '\'' +
                ", company='" + company + '\'' +
                ", companyInfo='" + companyInfo + '\'' +
                ", author='" + author + '\'' +
                ", publishTime=" + publishTime +
                '}';
    }
}
