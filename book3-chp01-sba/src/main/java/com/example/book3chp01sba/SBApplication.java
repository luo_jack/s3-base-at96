package com.example.book3chp01sba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
@Controller
public class SBApplication {

    public static void main(String[] args) {
        SpringApplication.run(SBApplication.class,args);
    }

    @RequestMapping("/hello2")
    @ResponseBody
    public String hello(){
        return "hello,world";
    }
}
