package com.example.book3chp01sba.car.utils.exception;

import com.example.book3chp01sba.car.utils.ReponseResult;
import com.example.book3chp01sba.car.utils.enums.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 全局统一异常处理
 */
@RestControllerAdvice
@Slf4j  // 生成log日志对象
public class GlobalExceptionHandle {
//    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandle.class);
    /**
     * 未知异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ReponseResult unknowExceptionHandler(Exception e) {
        log.error(e.getMessage());
        return new ReponseResult(ResultCodeEnum.UNKNOW_ERROR.getCode(), e.getMessage());
    }
}
