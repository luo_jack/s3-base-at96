package com.example.book3chp01sba.car.entity;

import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CarType {

    private int tid;

    private String name;

    public CarType(String name) {
        this.name = name;
    }
    
}
