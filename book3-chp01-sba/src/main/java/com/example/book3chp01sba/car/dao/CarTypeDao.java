package com.example.book3chp01sba.car.dao;


import com.example.book3chp01sba.car.entity.CarType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CarTypeDao {

    List<CarType> getAll();

    CarType getById(@Param("id") int id);

    int add(CarType carType);

    int update(CarType carType);

    int delete(@Param("id") int id);
}
