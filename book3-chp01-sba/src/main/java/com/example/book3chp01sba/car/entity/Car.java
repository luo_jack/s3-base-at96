package com.example.book3chp01sba.car.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Car {

    private int carId;
    private String carName;
    private double carPrice;
    private String carMileage;
    private Date carBuyTime;
    private int carType;
    private int carStatus;


}
