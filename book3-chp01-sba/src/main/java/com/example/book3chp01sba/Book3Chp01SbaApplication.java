package com.example.book3chp01sba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Book3Chp01SbaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Book3Chp01SbaApplication.class, args);
    }

}
