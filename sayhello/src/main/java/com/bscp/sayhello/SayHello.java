package com.bscp.sayhello;

public class SayHello {

    private String helloMsg;

    public String sayHello(){
        return "hello," + helloMsg;
    }

    public String getHelloMsg() {
        return helloMsg;
    }

    public void setHelloMsg(String helloMsg) {
        this.helloMsg = helloMsg;
    }
}
