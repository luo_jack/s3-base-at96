package com.bscp.sayhello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@ConditionalOnClass({SayHello.class})
@EnableConfigurationProperties({SayHelloProperties.class})
@ConditionalOnProperty(prefix = "hello",value = "helloMsg",matchIfMissing = true)
public class SayHelloAutoConfiguration {

//    @Autowired
    @Resource
    private SayHelloProperties sayHelloProperties;

    @Bean
    @ConditionalOnMissingBean({SayHello.class})
    public SayHello sayHello(){
        SayHello sayHello = new SayHello();
        sayHello.setHelloMsg(sayHelloProperties.getHelloMsg());
        return sayHello;
    }
}
