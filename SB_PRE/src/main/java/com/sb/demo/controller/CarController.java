package com.sb.demo.controller;

import com.sb.demo.dao.CarDao;
import com.sb.demo.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    CarDao carDao;

    @GetMapping("/list")
    public Object getCarByName(@RequestParam(name = "name", defaultValue = "") String carName){
        List<Car> cars = carDao.getCarByName(carName);
        return cars;
    }
}
