package com.sb.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbPreApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbPreApplication.class, args);
    }

}
