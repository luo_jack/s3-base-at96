package com.sb.demo.dao;


import com.sb.demo.entity.CarType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CarTypeDao{

    CarType getById(int tid);
}
