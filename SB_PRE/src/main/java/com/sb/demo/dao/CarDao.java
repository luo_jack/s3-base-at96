package com.sb.demo.dao;


import com.sb.demo.entity.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CarDao {

    List<Car> getCarByName(String carName);

    List<Car> getCarByPojo(Car car);

    List<Car> getCarByMap(Map<String,Object> carMap);

    List<Car> getCarByParams(@Param("carName") String carName,
                             @Param("carType") int carType);

    int updateCarPrice(@Param("carId") int carId,
                       @Param("carPrice") double carPrice);
}
