package com.sb.demo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Car {

    private int carId;
    private String carName;
    private double carPrice;
    private String carMileage;
    private Date carBuyTime;
    private int carType;
    private int carStatus;

    private CarType carTypeObj;

    public CarType getCarTypeObj() {
        return carTypeObj;
    }

    public void setCarTypeObj(CarType carTypeObj) {
        this.carTypeObj = carTypeObj;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public double getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(double carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarMileage() {
        return carMileage;
    }

    public void setCarMileage(String carMileage) {
        this.carMileage = carMileage;
    }

    public Date getCarBuyTime() {
        return carBuyTime;
    }

    public void setCarBuyTime(Date carBuyTime) {
        this.carBuyTime = carBuyTime;
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public int getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(int carStatus) {
        this.carStatus = carStatus;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", carName='" + carName + '\'' +
                ", carPrice=" + carPrice +
                ", carMileage='" + carMileage + '\'' +
                ", carBuyTime=" + carBuyTime +
                ", carType=" + carType +
                ", carStatus=" + carStatus +
                ", carTypeObj=" + carTypeObj +
                '}';
    }
}
