package com.login.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> jdkRedisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String, Object> redis=new RedisTemplate<>();
        redis.setConnectionFactory(factory);
        redis.setKeySerializer(RedisSerializer.string());
        redis.setValueSerializer(RedisSerializer.json());
        return redis;
    }
}
