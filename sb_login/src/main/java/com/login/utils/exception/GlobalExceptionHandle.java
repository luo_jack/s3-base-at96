package com.login.utils.exception;


import com.login.utils.ResponseResult;
import com.login.utils.enums.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 全局统一异常处理
 */
@RestControllerAdvice
@Slf4j  // 生成log日志对象
public class GlobalExceptionHandle {
//    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandle.class);
    /**
     * 未知异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseResult unknowExceptionHandler(Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return new ResponseResult(ResultCodeEnum.UNKNOW_ERROR.getCode(), e.getMessage());
    }
}
