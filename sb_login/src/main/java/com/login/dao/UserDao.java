package com.login.dao;

import com.login.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {

    User getUserByUsername(String username);
}
