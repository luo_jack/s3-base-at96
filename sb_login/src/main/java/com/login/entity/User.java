package com.login.entity;

import lombok.Data;

@Data
public class User {

    private int userId;

    private String username;

    private String nickname;

    private String passwordHash;

}
