package com.login.service;

import com.login.dao.UserDao;
import com.login.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public User login(String username,String password){
        User user = userDao.getUserByUsername(username);
        if(user == null){
            return null;
        }
        if(!user.getPasswordHash().equals(password)){
            return null;
        }
        return user;
    }
}
