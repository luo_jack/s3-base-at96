package com.login.resp;

import lombok.Data;

@Data
public class LoginUserResp {

    private int userId;

    private String username;

    private String nickname;

    private String token;
}
