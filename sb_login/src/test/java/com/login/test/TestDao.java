package com.login.test;

import com.login.dao.UserDao;
import com.login.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestDao {

    @Autowired
    UserDao userDao;

    @Test
    void test1(){
        User user = userDao.getUserByUsername("user2");
        System.out.println(user);
        User user2 = userDao.getUserByUsername("user22222");
        System.out.println(user2);
    }
}
