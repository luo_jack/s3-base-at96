package com.reflection.test;

import java.lang.reflect.Field;

public class Test3 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        String className = "com.reflection.test.Student";
        Class studentClz = Class.forName(className);
        Object obj = studentClz.newInstance();
        Field infoField = studentClz.getDeclaredField("info");
        infoField.set(obj,"一个好汉");

        Field idField = studentClz.getDeclaredField("id");
        idField.setAccessible(true);
        idField.set(obj,1002);

        Field nameField = studentClz.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(obj,"tom");

        System.out.println(obj);
    }
}
