package com.reflection.test;

import java.lang.reflect.Field;

public class Test2 {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Student student = new Student();

        Class studentClz = student.getClass();
        Field infoField = studentClz.getDeclaredField("info");
        infoField.set(student,"一个好汉");

        Field idField = studentClz.getDeclaredField("id");
        idField.setAccessible(true);
        idField.set(student,1001);

        Field nameField = student.getClass().getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(student,"jack");
        System.out.println(student);
    }
}
