package com.reflection.test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test1 {

    public static void main(String[] args) throws ClassNotFoundException {
        //描述类的对象(类对象)
        Class studentClz = null;
        //1.调用类或接口的class属性
//        studentClz = Student.class;
        //2.调用类或接口实例的getClass()方法
//        Student student = new Student();
//        studentClz = student.getClass();
        //3.使用Class.forName()方法
        studentClz = Class.forName("com.reflection.test.Student");
        //获取字段
        Field[] fields = studentClz.getDeclaredFields();
        for (Field field:fields){
            System.out.println(field.getName()+"\t"+field.getType()+"\t"+field.getModifiers());
        }
        System.out.println("-------------------------");
        //获取方法
        Method[] methods = studentClz.getDeclaredMethods();
        for (Method method:methods){
            System.out.println(method);
        }
    }
}
