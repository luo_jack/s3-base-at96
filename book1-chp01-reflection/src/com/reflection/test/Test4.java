package com.reflection.test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test4 {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        String className = "com.reflection.test.Student";
        Class studentClz = Class.forName(className);
        Object obj = studentClz.newInstance();
        Field infoField = studentClz.getDeclaredField("info");
        infoField.set(obj,"一个好汉");

        //获取方法
        Method method1 = studentClz.getDeclaredMethod("setId",int.class);
        method1.invoke(obj,2001);

        Method method2 = studentClz.getDeclaredMethod("setName",String.class);
        method2.invoke(obj,"莫妮娜");
        System.out.println(obj);
    }
}
